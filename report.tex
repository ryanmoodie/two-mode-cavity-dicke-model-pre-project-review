\documentclass[pra,twocolumn,nofootinbib]{revtex4-1}

\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amsmath,amssymb,physics}
\usepackage{braket}

\newcommand\TinyGap{\vspace{1mm}}
\newcommand\SmallerGap{\vspace{5mm}}
\newcommand\SmallGap{\vspace{10mm}}
\newcommand\MediumGap{\vspace{20mm}}
\newcommand\LargeGap{\vspace{30mm}}
\newcommand\SmallFigureWidth{0.4\linewidth}

\graphicspath{{images/}}

\begin{document}

	\begin{titlepage}
		
		\centering
		\Large

		\textbf{PH5103}

		\MediumGap

		\textbf{MPhys Project in Theoretical Physics}

		\SmallerGap

		Pre-Project Review

		\LargeGap

		{\Huge \textsc{simulating continuous symmetry breaking with cold atoms in optical cavities}}

		\MediumGap

		{\LARGE Ryan I. Moodie}

		\vfill

		\textit{Supervisors:}

		\TinyGap

		Jonathan Keeling
		
		Kyle Ballantine

		\LargeGap

		Word count: 2495

		\SmallGap

		\today

		\pagebreak

	\end{titlepage}

	\begin{abstract}

		The Dicke model, describing a system of two-level atoms interacting with a single cavity mode, exhibits an intriguing zero-temperature phase transition. Experimental realisation of an effective Dicke model operating in the transition regime is possible through a scheme involving multilevel atoms and Raman transitions. This has allowed exploration of the transition, which is associated with a spontaneously broken discrete symmetry. We propose to theoretically investigate an extension of this scheme with a multimode cavity by mapping out its complete phase diagram in mean-field theory. This scheme involves each Raman channel being mediated by different modes, allowing exploration of a continuous $U(1)$ symmetry breaking.  

	\end{abstract}

	\maketitle

	\section{Motivation}

		Phase transitions are extremely common phenomena. Since the dawn of quantum mechanics, our knowledge of exotic phases of matter and phase transitions has burgeoned, allowing new insight into the fundamental physics of the universe and great progress in applied physics. Specifically, understanding of phase transitions in condensed matter theory is vital to understanding matter itself. 

		Symmetry is one of the most intrinsic ways in which we understand the universe and an extremely useful property in physics. Phases of matter can be described in terms of their symmetries and many phase transitions exhibit symmetry breaking. To illustrate, consider water. Liquid water is homogeneous, i.e. it is symmetric such that the spatial geometry of the system is independent of continuous changes of location. On freezing, this continuous translational invariance is lost: the crystalline structure of ice has only discrete spatial symmetry. 

		Simple, well-controlled systems provide the means by which to probe the behaviour of quantum systems, such as mapping their phase diagrams, creating an immediate link between experiment and the theoretical models describing the underlying physics. Cavity quantum electrodynamics (QED) with ultracold atoms provides many such systems, with one of the simplest realisable and interesting examples being that of two-level atoms interacting with light in a cavity. The investigation of this coupled light-matter system, with modern experiments allowing study of the applicability of generic models and feeding new theory, provides a route to exploring collective behaviour --- symmetry breaking and phase transitions --- in quantum systems.

	\section{Dicke model}

		The Dicke model \cite{Radiation1954} is the simplest description of an ensemble of many atoms, assumed to have only two energy levels, collectively interacting with a single quantised mode of an electromagnetic field in a cavity. The Dicke Hamiltonian is\footnote{Throughout this paper, H.c. denotes the Hermitian conjugate and equations are expressed with units such that $\hbar=1$.}:

		\begin{equation}
			\hat{H} = \omega \hat{a}^{\dagger} \hat{a} + \sum_{i} \bigg [ \omega_{0} \hat{s}^{z}_{i} + g \Big ( \hat{a}^{\dagger} \hat{s}^{-}_{i} + \hat{a}^{\dagger} \hat{s}^{+}_{i} + H.c. \Big ) \bigg ]
			\label{dicke-hamiltonian}
 		\end{equation}
 		describing the energy of the cavity mode, the atoms and the light-matter interaction, respectively. $\omega$ is the cavity mode frequency, $\omega_{0}$ is the frequency splitting between the two atomic levels, $g$ is the light-matter (dipole) coupling strength, $i$ labels atoms and $\hat{a}$ and $\hat{a}^{\dagger}$ are the annihilation and creation operators for the cavity mode. Considering the two-level atoms like spin-$\frac{1}{2}$ particles, $\hat{s}^{z}_{i}$ is the $z$ spin operator and $\hat{s}^{-}_{i}$ and $\hat{s}^{+}_{i}$ are the lowering and raising operators. 

 		The model predicts a phase transition at a critical dipole coupling strength to a coherent state in which collective effects cause enhancement of radiation and thus macroscopic occupation of the cavity mode: superradiance \cite{Wang1973,Models1973,Emary2003,Emary2003a,Hepp1973a}. However, it was shown that this is not realisable in a closed system \cite{Rzazewski1975,Nataf2010,Bialynicki-Birula1979}. The problem lies in the approximations which were historically chosen to derive this Hamiltonian. We limit to two-level atomic systems and a single cavity mode such that the wavelength of light resonant with the atomic transition is much greater than the characteristic scale of the atoms, i.e. the dipole approximation. This yields Eq.~(\ref{dicke-hamiltonian}) with additionally a diamagnetic term of second order in the magnetic vector potential field, denoted the $\hat{A}^{2}$ term. Under the assumption of dilute atoms, we neglect this additional term to obtain the Dicke Hamiltonian. However, in the regime of critical dipole coupling, there is a high atomic density and thus the term must be included.	In this case, the superradiant phase transition does not appear. Nevertheless, modern investigation of open systems has provided a route to realising the transition. 

	\section{Open quantum systems}

		Phase transitions can be defined by a change of the favoured ground state, with this stable, steady ground state (i.e. a stationary point of the dynamics) describing the phase of matter. In a closed quantum system, the ground state is simply that which minimises the energy of the system. The analogue of this in an open quantum system is called an attractor. Through the driven and dissipative effects of open systems, attractors can also correspond to maxima of energy. This leads to exotic behaviour in the phase diagram: regions where the steady state maximises the energy; bistability of two different stable attractors which overlap in parameter space; or limit cycles which are not steady but instead oscillatory solutions \cite{Chan2015}. The limit cycles are particularly novel, with their closed system analogue --- often called ``time crystals'' --- never having been observed experimentally \cite{PhysRevLett.109.160401}.

	\section{Effective Dicke model}

		A recent theoretical proposal of an effective Dicke model \cite{Dimer2007} has motivated much exploration of collective behaviour in open quantum systems through cavity QED. The scheme uses interactions which are not sensitive to the omission of the $\hat{A}^{2}$ term to realise an effective nonequilibrium Dicke Hamiltonian based on constructing effective two-level systems using multilevel atoms and pump-and-cavity-mediated Raman transitions (Fig.~\ref{raman-scheme}) following studies of Raman scattering such as Ref.~\cite{Raymer1981}. 

		\begin{figure}
			\centering
			\includegraphics[width=\SmallFigureWidth]{raman-scheme}
			\caption{Atomic energy level scheme, taken from Ref.~\cite{Dimer2007}. $\ket{0}$ and $\ket{1}$ are two stable atomic ground states, forming the two energy levels of the effective two-level atomic system (for example two sublevels in the hyperfine structure) with frequency splitting $\omega_{1}$. $\ket{s}$ and $\ket{r}$ are the excited electronic states. The detuning of the pump from the transition frequency, $\Delta_{s}$ and $\Delta_{r}$, ensures that the excited states are not populated. $\Omega_{s}$ and $\Omega_{r}$ are Rabi frequencies (frequency of oscillation of the electron population between the two levels of the atomic transition in the light field) and $g_{s}$ and $g_{r}$ are dipole coupling strengths, associated with the shown transitions.}
			\label{raman-scheme}
		\end{figure}

		The system is pumped by a pair of lasers, allowing the parameters of the system to be tuned by the frequencies and intensities of the lasers. The states of an effective two-level system are coupled by a pair of Raman channels, allowing a path for transitions between $\ket{0}$ and $\ket{1}$. The lasers drive the transitions between ground and excited states, $\ket{1} \leftrightarrow \ket{r}$ and $\ket{0} \leftrightarrow \ket{s}$. Excitation is induced by absorption and de-excitation by stimulated emission (assuming strong lasers, spontaneous emission is neglected). The optical cavity mode mediates the $\ket{r} \leftrightarrow \ket{0}$ and $\ket{s} \leftrightarrow \ket{1}$ transitions. 

		Assuming large detuning, $\Delta_{s}$ and $\Delta_{r}$, the excited states can be adiabatically eliminated. The system can then be described by an effective Dicke Hamiltonian:

		\begin{equation}
			\hat{H} = \omega \hat{a}^{\dagger} \hat{a} + \sum_{i} \bigg [ \omega_{0} \hat{s}^{z}_{i} + g \Big ( \hat{a}^{\dagger} \hat{s}^{-}_{i} + \hat{a}^{\dagger} \hat{s}^{+}_{i} + H.c. \Big ) + U \hat{s}^{z}_{i} \hat{a}^{\dagger} \hat{a} \bigg ]
			\label{effective-dicke-hamiltonian}
 		\end{equation}
 		where parameters are associated with optical frequency shifts and Raman transition rates rather than those of the photons and dipole coupling, e.g. $\omega$ is now related to the difference between the cavity mode and pump frequencies. The cavity frequency is shifted depending on whether the atoms are in the state $\ket{0}$ or $\ket{1}$ and in turn the atomic energies are affected by the cavity frequency, the physics involving a virtual process where ground state electrons interacts with photons to excite and then de-excite. This feedback effect is described by the additional $U$ term.

 		\section{Phase transition}

		\begin{figure}
			\centering
			\includegraphics[width=\linewidth]{phase-diagram}
			\caption{Phase diagram of the Raman scheme system, adapted from Ref.~\cite{Baumann2010}. The experimental results (colourmap) show a phase transition to the superradiant phase in good agreement with the theoretical model in mean-field theory (dashed line). (No data was taken for the black region.)}
			\label{phase-diagram}
		\end{figure}

		Almost 40 years after the initial prediction \cite{Radiation1954}, recent experiments on Bose-Einstein condensates of ultracold atoms in optical cavities have observed this transition \cite{Baumann2010} (Fig.~\ref{phase-diagram}) following the discussed theoretical proposal. The superradiant transition has been found to coincide with the emergence of a state of self-organisation of the atoms \cite{Baumann2010,Szirmai2010,Nagy2008}, complemented by previous studies \cite{Black2003}. This has prompted much related work \cite{Mottl2012,Kollar2016,Brennecke2013}.

		\begin{figure}
			\centering
			\includegraphics[width=\linewidth]{chequerboard}
			\caption{Experimental system (taken from Ref.~\cite{Bhaseen2012}) showing cold atoms (red) and light fields (blue) in a cavity with pumping and loss. (a) Below threshold pump power. (b) Above threshold.}
			\label{chequerboard}
		\end{figure}

		If the spatial behaviour of the system of Ref.~\cite{Baumann2010} (Fig.~\ref{chequerboard}) is considered, it is apparent that the wavefunction of the transverse pump will form a set of nodes in the z-direction. Similarly, the cavity field sets up a standing wave in the x-direction. Below the threshold pumping strength, there is only macroscopic occupation of the pump field and thus atoms of the condensate align in the anti-nodes of the pump wavefunction (Fig.~\ref{chequerboard} (a)). 

		Above a critical pump power, the superradiant phase is achieved and there is also macroscopic occupation of the cavity field. As we are assuming the cavity light has come from the pump through scattering by the atoms and so the pump and cavity fields are in phase, a grid pattern is formed (Fig.~\ref{chequerboard} (b)). It is energetically favourable for the atoms to sit in the sites of the anti-nodes of the pattern. However, there are two possible wavefunctions that the atomic condensate may assume: either occupying all the odd or even sites of the grid, forming a chequerboard. This chequerboard forms Bragg planes which scatter the pump light into the cavity, meaning there is light in the cavity provided the scattering has a higher rate than the cavity loss. This forms a self-sustaining cycle: light in the cavity induces the chequerboard, and the chequerboard induces light in the cavity.

	\section{Symmetry breaking}

		While this lattice is imposed by design, the even and odd chequerboards have equal probability. The condensate assumes one of the possible wavefunctions through initial fluctuations then self-organisation, as described above. As each chequerboard pattern corresponds to a given cavity mode phase, a discrete $\mathbb{Z}_{2}$-symmetry\footnote{$\mathbb{Z}_{2}$ refers to the cyclic group of order two, corresponding to the group of parity.} is spontaneously broken \cite{Baumann2011} in the relative phase between the pump and cavity modes. This can equally be understood as a spatial symmetry breaking in the atomic density. With such symmetry breaking at a phase transition being so fundamental and also notoriously difficult to test experimentally due to external fluctuations, this scheme has attracted significant interest. 

		Artificial systems exhibiting a continuous $U(1)$ symmetry\footnote{$U(1)$ corresponds to the multiplicative group of all complex numbers of unit amplitude.} are the focus of much attention owing to their connection to the physics of the Anderson-Higgs mechanism \cite{Higgs1964}. It is possible to design an extension to the effective Dicke model to explore $U(1)$ symmetry \cite{Leonard2016,Baksic2014}. In this case, two cavities are overlapped such that the chequerboard potentials compete. The two parity symmetries of the cavity light fields are combined to form one $U(1)$ symmetry (Fig.~\ref{compete}) which exists along a sharp line in parameter space (Fig.~\ref{phase-diagram-continuous}).

		\begin{figure}
			\centering
			\includegraphics[width=\linewidth]{compete}
			\caption{Taken from Ref.~\cite{Leonard2016}. The field amplitudes of the cavity modes, $\alpha_{i}$, are the order parameters of the system. The $d$-periodic interference potential moves continuously in the x-direction when varying the angle in the $\alpha_{1}$-$\alpha_{2}$ plane, as shown. On transition, the system state might assume any point on the circumference drawn, i.e. any angle in the $\alpha_{1}$-$\alpha_{2}$ plane, giving $U(1)$ symmetry. If one cavity is ``switched off'', we are confined to a single axis and so the state can assume only two points: this yields the $\mathbb{Z}_{2}$-symmetry of the previous experiment.}
			\label{compete}
		\end{figure}

		\begin{figure}
			\centering
			\includegraphics[width=\linewidth]{phase-diagram-continuous}
			\caption{Phase diagram of the two-cavity system, adapted from Ref.~\cite{Leonard2016}, showing mean photon population of cavity $i$, $\tilde{n}_{i}$, against pump-cavity detunings $\Delta_{i}$. Panels (a) and (b) show occupation of the cavity one mode and cavity two mode respectively. Dashed lines delineate phase boundaries predicted by mean-field theory, showing good agreement with experimental data (colourmap). The lower left region is the normal phase, where neither cavity has macroscopic mode occupation. The superradiant phase in the mode of cavity one corresponds to the rightmost region, and similarly for cavity two in the uppermost region. The regime of $U(1)$ symmetry exists only along the diagonal section of the dashed line.}
			\label{phase-diagram-continuous}
		\end{figure}

	\section{Two-mode cavity}

		\begin{figure}
			\centering
			\includegraphics[width=\SmallFigureWidth]{raman-scheme-continuous}
			\caption{Atomic energy level scheme as in Fig.~\ref{raman-scheme}, but for the two-mode extension where modes are nondegenerate in energy and mediate nondegenerate $\ket{r} \leftrightarrow \ket{0}$ and $\ket{s} \leftrightarrow \ket{1}$ transitions. Modes could alternatively be distinguished by polarisation. Adapted from Ref.~\cite{Dimer2007}.}
			\label{raman-scheme-continuous}
		\end{figure}		

		This project proposes an alternative extension allowing two different transverse modes in a single multimode cavity which exhibits a phase transition associated with continuous $U(1)$ symmetry breaking in the relative phase of the two modes. One mode mediates each leg of the Raman transition (Fig.~\ref{raman-scheme-continuous}), allowing channel-specific tuning. In contrast to the previous experiment, the regime displaying $U(1)$ symmetry will not require a specific relation between the detunings of the two modes and so be accessible under an extended range of parameters. The system will be explored, in particular mapping out its complete phase diagram, analogous to the study of a similar model \cite{Bhaseen2012,Keeling2010}.

		The general Hamiltonian of this two-mode Dicke model is: 

		\begin{multline}
			\hat{H} = \omega_{a} \hat{a}^{\dagger} \hat{a} + \omega_{b} \hat{b}^{\dagger} \hat{b} + \sum_{i} \bigg [ \omega_{0} \hat{s}^{z}_{i} 
			+ g_{a} \Big ( \hat{a}^{\dagger} \hat{s}^{-}_{i} + H.c. \Big ) \\
			+ g_{b} \Big ( \hat{b}^{\dagger} \hat{s}^{+}_{i} + H.c. \Big )
			+ g_{a}^{\prime} \Big ( \hat{a}^{\dagger} \hat{s}^{+}_{i} + H.c. \Big )
			+ g_{b}^{\prime} \Big ( \hat{b}^{\dagger} \hat{s}^{-}_{i} + H.c. \Big ) \\
			+ U s_{i}^{z} \Big ( \hat{a}^{\dagger} \hat{a} - \hat{b}^{\dagger} \hat{b} \Big )
			\bigg ]
			\label{two-mode-hamiltonian}
 		\end{multline}
 		where $a$ and $b$ label the two cavity modes and the primed terms allow dipole coupling for different transition routes to differ. With cavity loss $\kappa$ on both modes, the equations of motion of the system can be found using the Lindblad equation:

		\begin{equation}
			\frac{d}{d t} \hat{\rho} = - i [\hat{H}, \hat{\rho}] + \frac{\kappa}{2} \mathcal{L}[\hat{a}] + \frac{\kappa}{2} \mathcal{L}[\hat{b}]
			\label{lindblad-equation}
		\end{equation}
		where $i$ labels cavity modes and the cavity loss term is described by the Lindblad superoperator $\mathcal{L}[\hat{X}]$, given by:

		\begin{equation}
			\mathcal{L}[\hat{X}] = 2 \hat{X} \hat{\rho} \hat{X}^{\dagger} - \hat{X}^{\dagger} \hat{X} \hat{\rho} - \hat{\rho} \hat{X}^{\dagger} \hat{X} 
			\label{lindblad-superoperator}
		\end{equation}

		Working in mean-field theory, we will write the classical equations of motion with respect to the expectation values of the variables, following from Eq.~(\ref{lindblad-equation}). This gives a set of $N$ first order in time differential equations for $N$ variables. 

		At this point, there are two ways to proceed. The first is possible at least where we expect fixed points of the dynamics exist: find the steady state of the system by setting the time derivatives to zero and simultaneously solving the equations, then look at the linear stability of these to check for stability and find phase boundaries. This involves adding a small fluctuation to the steady state value of each variable in the equations of motion, discarding fluctuation terms of higher than linear order, then seeing if the solution is stable. Stability indicates the phase of the system at that point in parameter space has been found. This is likely to be analytic for the initial simple cases, algebraically solving the systems of equations by hand, and semi-analytic in the more general cases, requiring diagonalisation of relatively small matrices. For an unstable solution, we will proceed with the second option: brute-force numerics. We will numerically time evolve the equations of motion to determine the system behaviour and categorise the state observed. To map out the entire phase diagram, this process will be repeated at every point in parameter space. 

		If there is time remaining, we will begin exploring the system beyond mean-field theory. In this case, the quantum dynamics can be modelled by a quantum master equation; a new numerical technique \cite{Kirton2016} may benefit the calculation. This describes the time evolution of the system density matrix, additionally describing coherences as well as populations. It is unclear before performing the calculations what this may add to our understanding, but may be used to clarify any uncertainties that arise in the mean-field theory approach. 

	\section*{Conclusion}

		Open quantum systems which can be modelled by the effective Dicke model using the Raman scheme and its various extensions are extremely useful testbeds with which to explore zero-temperature phase transitions and associated symmetry breaking. This project will theoretically investigate a new extension which exhibits a continuous $U(1)$ symmetry breaking in what holds promise to be a more easily controlled system than previous experiments.

	\bibliography{report}

\end{document}

